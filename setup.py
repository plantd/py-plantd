# coding=utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

from setuptools import find_packages
from setuptools import setup

setup(
    name='py-plantd',
    version='0.0.1',
    author='Geoff Johnson',
    author_email='geoff.jay@gmail.com',
    description='Plantd library for creating distributed control systems',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'simplejson~=3.17.0',
        'py-zapi @ git+https://gitlab.com/plantd/py-zapi.git@master',
    ],
    tests_require=[
        'nose',
    ],
)
