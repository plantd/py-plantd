[![codecov](https://codecov.io/gl/plantd/py-plantd/branch/master/graph/badge.svg)](https://codecov.io/gl/plantd/py-plantd)

---

# Python Plantd Library

Native `python` library for `plantd` development.

## Develop

```sh
python3 -m venv .venv
source .venv/bin/activate
pip install -r requirements.txt -e .
pre-commit install
```
