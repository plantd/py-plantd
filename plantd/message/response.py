# coding=utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

from dataclasses import dataclass

import jsons


@dataclass
class Response(object):

    def to_json(self):
        return jsons.dump(self)

    @classmethod
    def from_json(cls, json: str):
        return jsons.load(json, cls)
