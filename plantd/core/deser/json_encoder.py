# coding=utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import inspect
import json


# FIXME: not really sure about encoder/decoder method yet
class ObjectEncoder(json.JSONEncoder):
    """
    Object serializer.

    Example: `json.dumps(Foo(), cls=ObjectEncoder, indent=2, sort_keys=True)`
    """

    def default(self, obj):
        if hasattr(obj, 'to_json'):
            return self.default(obj.to_json())
        elif hasattr(obj, '__dict__'):
            d = dict(
                (key, value)
                for key, value in inspect.getmembers(obj)
                if not key.startswith('__') and not
                inspect.isabstract(value) and not
                inspect.isbuiltin(value) and not
                inspect.isfunction(value) and not
                inspect.isgenerator(value) and not
                inspect.isgeneratorfunction(value) and not
                inspect.ismethod(value) and not
                inspect.ismethoddescriptor(value) and not
                inspect.isroutine(value)
            )
            return self.default(d)
        return super(ObjectEncoder, self).default(obj)
