# coding=utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import json


# FIXME: not really sure about encoder/decoder method yet
# FIXME: this has nothing to do with anything yet, just an example
class ObjectDecoder(json.JSONDecoder):
    """
    Object deserializer.

    Example: `json.loads(s, cls=ObjectDecoder)`
    """

    def __init__(self, *args, **kwargs):
        json.JSONDecoder.__init__(
            self, object_hook=self.object_hook, *args, **kwargs
        )

    def object_hook(self, obj):
        # if '_type' not in obj:
        # return obj
        # type = obj['_type']
        return obj
