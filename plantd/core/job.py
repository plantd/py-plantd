# coding=utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

from dataclasses import dataclass

import jsons

from plantd.model import Node


@dataclass
class Job(Node):

    def __init__(self, value=None, properties=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.value = value
        self.properties = properties

    def to_json(self):
        return jsons.dump(self)

    @classmethod
    def from_json(cls, json: str):
        return jsons.load(json, cls)
