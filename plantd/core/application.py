# coding=utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import logging.config
import os

import yaml


logger = logging.getLogger(__name__)


class Application(object):

    def __init__(self):
        # TODO: move this somewhere useful
        self.ctx = {
            'running': False,
        }

        # initialize application base
        try:
            filename = os.getenv('CONFIG_FILE', 'config.yaml')
            # TODO: read more configuration than just logging
            with open(filename, 'r') as f:
                config = yaml.safe_load(f.read())
                logging.config.dictConfig(config)
        except FileNotFoundError:
            logging.error(f'Unable to read the configuration file at {filename}')

        # FIXME: this doesn't use the configuration
        logger.debug('Finished application base initialization')

    @property
    def running(self) -> bool:
        return self.ctx['running']

    # XXX: one possible idea would be to add signals and control things by
    #  attaching a callback to the receiver and raising here
    # @running.setter
    # def running(self, running: bool) -> None:
        # if self.ctx.running == running:
        # raise ValueError('Application is already running')
        # self.ctx.running = running
