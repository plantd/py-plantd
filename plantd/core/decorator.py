# coding=utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import logging

# from . import Application

logger = logging.getLogger(__name__)


def module(func):
    def main(args):
        # app = Application()
        logger.debug('Module setup complete')
        func(args)
    return main
