# coding=utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import jsons


class Object(object):

    objects: set = {}
    properties: set = {}

    def __init__(self):
        pass

    def to_json(self):
        return jsons.dump(self)

    @classmethod
    def from_json(cls, json: str):
        return jsons.load(json, cls)
