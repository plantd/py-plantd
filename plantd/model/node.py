# coding=utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import uuid
from dataclasses import dataclass


@dataclass
class Node(object):

    id: str

    def __init__(self, *args, **kwargs):
        self.id = kwargs.get('id') if kwargs.get('id') is not None else uuid.uuid4()

    def __str__(self):
        return f'id: {self.id}'
