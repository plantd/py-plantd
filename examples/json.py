# coding=utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

from plantd.model import Object


obj = Object()
foo = Object()
obj.objects = {foo}

json = obj.to_json()

print(json)

obj2 = Object.from_json(json)

# Make sure we have the correct thing
assert isinstance(obj2, Object)
assert obj2.__dict__ == obj.__dict__
