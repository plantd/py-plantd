# coding=utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import sys

from plantd.core import module


# TODO: figure out how to pass things like application in
@module
def main(args):
    return 0


if __name__ == '__main__':
    result = main(sys.argv)
    sys.exit(result)
