# coding=utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import unittest

from nose.tools import eq_

from plantd.message import Response


class TestResponse(unittest.TestCase):

    def setUp(self):
        self.response = Response()

    def test_json_serialize(self):
        eq_(self.response.to_json(), {})
