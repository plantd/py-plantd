# coding=utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import unittest

from nose.tools import eq_

from plantd.message import Request


class TestRequest(unittest.TestCase):

    def setUp(self):
        self.request = Request()

    def test_json_serialize(self):
        eq_(self.request.to_json(), {})
