# coding=utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import re


def valid_uuid(uuid: str, version=4):
    if version == 4:
        regex = re.compile(r'^[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}\Z', re.I)
        return bool(regex.match(uuid))
    return False
