# coding=utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import unittest


loader = unittest.TestLoader()
start_dir = 'tests'
suite = loader.discover(start_dir)

runner = unittest.TextTestRunner()
runner.run(suite)
