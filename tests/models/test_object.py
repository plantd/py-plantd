# coding=utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import unittest

from nose.tools import eq_


class TestObject(unittest.TestCase):

    def setUp(self):
        pass

    def test_foo(self):
        eq_('foo'.upper(), 'FOO')
