# coding=utf-8
from __future__ import absolute_import
from __future__ import unicode_literals

import unittest

from nose.tools import eq_
from nose.tools import ok_

from plantd.model import Node
from tests.utils.validators import valid_uuid


class TestNode(unittest.TestCase):

    def test_node(self):
        node = Node(id='foo')
        eq_(f'{node}', 'id: foo')
        uuid_node = Node()
        ok_(valid_uuid(f'{uuid_node.id}'))
